function ShowProgress
{
    param([int] $total_items, [int] $current_item_index, [system.diagnostics.stopwatch] $stopwatch)
    $stopwatch_progress_id = 42
    $max_ms_between_updates = 2000 # There should be no more than three second between UI updates

    #
    # Ideally the mod maps to one tick of the progress bar 
    # Tick count is roughly the console width minus 10
    #
    $console_width = $Host.UI.RawUI.WindowSize.Width
    $progress_bar_ticks = $console_width - 10
    if($progress_bar_ticks -le 0) {$progress_bar_ticks = 1} # head off a potential divide by zero
    [int] $calculated_mod_ticks = $total_items / $progress_bar_ticks

    # Create the stopwatch if it's not there
    try 
    {
        Get-Variable -Name "stopwatch" -Scope global -ErrorAction Stop  | Out-Null
    }
    catch 
    {
        New-Variable -Name "stopwatch" -Value $null -Scope global | Out-Null
        $global:stopwatch = [system.diagnostics.stopwatch]::StartNew() 
    }

    $update_ui = $false
    if($current_item_index -eq 1)
    {
        $update_ui = $true # Show the bar on first record
    }

    if($global:stopwatch.ElapsedMilliseconds -gt $max_ms_between_updates)
    {
        $update_ui = $true # max time elapsed between ui updates
    }

    if($($current_item_index) % $calculated_mod_ticks -eq 0)
    {
        $update_ui = $true # enough records have passed to show a new tick of the progress bar
    }

    if($update_ui)
    {
        Write-Progress -id $stopwatch_progress_id -Activity "Processing item $current_item_index of $total_items" -PercentComplete $($current_item_index / $total_items * 100)
        $global:stopwatch.Restart()
    }
}